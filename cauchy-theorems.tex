\documentclass[12pt]{amsart}
\begin{document}

\newtheorem{Thm}{Theorem}
\newtheorem{Cor}[Thm]{Corollary}
\newtheorem{Pro}[Thm]{Proposition}
\newtheorem{Lem}[Thm]{Lemma}
\theoremstyle{definition}
\newtheorem{Def}[Thm]{Definition}

\theoremstyle{remark}
\newtheorem{Rem}[Thm]{Remark}
\newtheorem{Exa}[Thm]{Example}
\newtheorem{Exs}[Thm]{Examples}

\def\1#1{\overline{#1}}
\def\4#1{\mathbb{#1}}

\title{Cauchy Theorem: Different Versions}
\maketitle

\section{Homotopy version}

\begin{Def}
A homotopy between two continuous maps $f_0,f_1\colon X\to Y$ between topological spaces $X$ and $Y$ is any continuous map 
$$
	\Phi\colon [0,1]\times X\to Y
$$ with 
$$
	\Phi(s, \cdot) = f_s(\cdot),
	\quad
	s = 0,1.
$$
\end{Def}

In particular, taking as $X$ the interval $[a,b]$ with endpoints ``glued together",
we obtain homotopy of closed paths:
\begin{Def}
A homotopy of closed paths $\gamma_0,\gamma_1\colon [a,b]\to Y$ 
into any topological space $Y$ is any continuous map 
$$
	\Phi\colon [0,1]\times [a,b]\to Y
$$ 
with 
$$
	\Phi(s, \cdot) = f_s(\cdot),
	\quad
	s = 0,1,
$$
and each $\Phi(s, \cdot)$ being closed path:
$$
	\Phi(\cdot ,a) = \Phi(\cdot ,b).
$$
\end{Def}




\begin{Thm}[Cauchy Theorem: Homotopy version]
Let $f$ be holomorphic in open set $\Omega\subset \4C$
and $\gamma_0,\gamma_1\colon [a,b]\to \Omega$ be homotopic
as closed paths, i.e.\ there exists a homotopy of closed paths
$$
	\Phi\colon [0,1]\times [a,b]\to \Omega
$$
with
\begin{equation}\label{closed}
	\Phi(s, \cdot) = \gamma_s(\cdot),
	\quad
	s = 0,1.
\end{equation}
Then
$$\int_{\gamma_0} f(z)\, d z = \int_{\gamma_1} f(z)\, d z .$$
\end{Thm}



\begin{proof}
The proof is based on the existence of decompositions
$$
	0=s_0 < s_1 <\ldots < s_n=1,
	\quad
	a=t_0 < t_1 <\ldots < t_m=b,	
$$
such that each 
$\Phi([s_{j-1},s_j]\times [t_{k-1},t_k])$
is contained in a disc $\Delta_{jk}$ in $\Omega$.
Recall that since each $\Delta_{jk}$ is star-shaped,
the restriction $f$ to it has an anti-derivative $F_{jk}$
and hence 
$$
	\int_\gamma f(z) \, dz = F_{jk}(\gamma(x)) - F_{jk}(\gamma(y))
$$
for any continuous path $\gamma\colon [x,y]\to \Delta_{jk}$.
In particular, the integral of $f$ over each path $\Phi(s_{j-1}, t)$, $t_{k-1}\le t\le t_k$,
equals the sum of the integrals over the $\Phi$-images other three other edges of the rectangular
$[s_{j-1}, s_j]\times[t_{k-1},t_k]$. Summing for $k=1,\ldots, m$, and taking into account 
cancellations for all vertical edges, we conclude
$$
	\int_{\Phi(s_{j-1}, \cdot)} f(z) \, dz = \int_{\Phi(s_{j}, \cdot)} f(z) \, dz,
$$
implying the conclusion.
Note the cancellation for the outside vertical edges follows from \eqref{closed}.
\end{proof}



\section{Chains and homology version}

Recall that for every integer $d\ge 0$, a {\em $d$-chain} in $\Omega$
is a formal finite linear combination $\phi = \sum_j a_j [\phi_j]$,
where 
\begin{equation}\label{cell}
\phi_j\colon \1\Delta \to \Omega
\end{equation}
is a $d$-cell,
i.e.\ continuous map from a closed $d$-simplex $\Delta$ to $\Omega$.
In particular, $0$-simplex is a point, $1$-simplex an interval, $2$-simplex a triangle.
The brackets $[\phi_j]$ are there to denote the equivalence class
of cells under reparamerizations of the simplex (continuous up to the boundary and sending boundary to boundary).

If $C_d$ is the vector space of $d$-chains in $\Omega$,
the {\em boundary operator}
$$\partial \colon C_d \to C_{d-1}$$
is the linear operator such that for any $d$-cell $\phi_j$ as in \eqref{cell},
$\partial [\phi_j]$ is the $(d-1)$-cell obtained by restricting $\phi_j$
to the oriented boundary of $\Delta$.
For instance, if $\Delta$ is a triangle, consider its oriented edges $I_1, I_2, I_3\subset \1\Delta$.
Then 
$$
	\partial [\phi_j] = \sum_{k=1}^3 [\phi_j|_{I_k}].
$$
Similarly, if $\Delta=[a,b]$ is an interval, 
$$
	\partial [\phi_j] = [\phi_j(b)] - [\phi_j(a)].
$$



\begin{Thm}[Cauchy Theorem: Homology version]
Let $f$ be holomorphic in open set $\Omega\subset \4C$
 and $c$ be an exact $1$-chain in $\Omega$,
i.e.\ $c = \partial \phi$ for some $2$-chain $\phi$ in $\Omega$.
Then
$$\int_c f(z)\, d z = 0.$$
\end{Thm}

\begin{proof}
Write $c=\partial \phi$, where $\phi=\sum a_j [\phi_j]$ is a finite sum of $2$-cells in $\Omega$,
where each $\phi_j$ is a continuous map from a closed triangle $\Delta_j$ into $\Omega$.
The boundary $\partial [\phi_j]$ of each cell can be represented by
null-homotopic closed path, since it is parametrized by the boundary 
of $\Delta_j$, which is null-homotopic inside the triangle $\Delta_j$.
Hence the homotopy version of Cauchy's theorem implies 
$$
	\int_{\partial [\phi_j]} f(z)\, d z = 0,
$$
from which the conclusion follows since $c = \sum a_j \partial [\phi_j]$.
\end{proof}

\begin{Exa}
If $c$ is not exact in $\Omega$, the integral $\int_c  f(z)\, d z$ may not be zero.
For instance, the integral of $1/z$ over the unit circle with $\Omega = \4C\setminus\{0\}$.
\end{Exa}

\begin{Exa}
If $f$ is not holomorphic, the integral $\int_c  f(z)\, d z$ may not be zero even when $c$ is exact.
For instance, the integral of $\bar z$ over the unit circle with $\Omega = \4C$.
\end{Exa}



\end{document}